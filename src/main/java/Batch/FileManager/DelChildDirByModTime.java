package Batch.FileManager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Batch.FileManager.setting.DelChildDirByModTimeSetting;

/**
 * 指定したディレクトリの子供の子供に、指定した時間(分)よりも古いファイルがあったら削除する。
 *
 * @author arimuraterutoshi
 *
 */
public class DelChildDirByModTime {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy'-'MM'-'dd' 'HH':'mm':'ss");

	public static void main(String args[]) {
		if (args.length < 1) {
			System.out.println("pleas input property file");
			return;
		}
		try {
			DelChildDirByModTime delBatch = new DelChildDirByModTime();
			delBatch.run(args[0].replaceAll("/", File.separator));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void run(String file_path) throws Exception {
		// 削除対象のディレクトリを取得
		DelChildDirByModTimeSetting setting = new DelChildDirByModTimeSetting(file_path);
		File course_upload_dir = new File(setting.getTarget_dir());
		List<File> del_files = findByModTime(course_upload_dir, setting.getDel_interval_min());
		for (File del_file : del_files) {
			if(setting.getTest_mode() == 1){
				System.out.println("削除対象ファイル: " + del_file.getName() + " 更新日: " + sdf.format(new Date(del_file.lastModified())));
			}else{
				file_delete_seq(del_file);
			}
		}
	}

	List<File> findByModTime(File target_dir_file, int del_interval_min) {
		Calendar now = Calendar.getInstance();
		long del_target_time = now.getTimeInMillis() - del_interval_min * 1000 * 60;
		List<File> ret = new ArrayList<File>();
		System.out.println("削除対象ファイル更新時間 " + sdf.format(new Date(del_target_time)));

		File[] company_dirs = target_dir_file.listFiles();
		for (File company_dir : company_dirs) {
			if (company_dir.isDirectory()) {
				for (File upload_course : company_dir.listFiles()) {
					if (upload_course.lastModified() < del_target_time) {
						ret.add(upload_course);
					}
				}
			}
		}
		return ret;
	}

	private static void file_delete_seq(File file) {
		if (file.exists() == false) {
			return;
		}

		if (file.isFile()) {
			file.delete();
		} else if (file.isDirectory()) {
			File[] child_files = file.listFiles();
			for (File child : child_files) {
				file_delete_seq(child);
			}
			file.delete();
		}
	}

}
