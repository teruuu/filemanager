package Batch.FileManager.setting;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import lombok.Data;

@Data
public class DelChildDirByModTimeSetting {

	/**
	 * 削除対象となるディレクトリ
	 */
	private String target_dir = "";

	/**
	 * 削除対象となるファイル判定(分）
	 * ここで指定した時間以上経過したファイルを削除対象とする。
	 */
	private int del_interval_min = 40;

	/**
	 * 1の場合は、削除対象に成るファイルをコンソールに出力するだけで削除はしない
	 */
	private int test_mode = 0;

	// 設定ファイルの読み込み
	public DelChildDirByModTimeSetting(String filePath) throws Exception{
		String file_path = filePath;
		FileInputStream fis = new FileInputStream(file_path);
		// 文字コード：UTF-8として、文字入力ストリームに変換する。
		InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		// 文字入力ストリームからプロパティリストを読み込む。
		Properties prop = new Properties();
		prop.load(br);

		// getPropertyメソッドでキーを指定し、値を取得する。
		this.target_dir = prop.getProperty("target_dir");
		this.del_interval_min = Integer.valueOf(prop.getProperty("del_interval_min"));
		this.test_mode = Integer.valueOf(prop.getProperty("test_mode"));
		fis.close();

		if(this.target_dir.equals("") || this.target_dir.equals("/")){
			throw new Exception();
		}
	}

}
